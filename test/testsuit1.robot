*** setting ***
Library    SeleniumLibrary         
 
Default Tags     sanity 

*** Test Cases ***
FirstTest
    [Tags]    SMoke 
    Log    Hello Blueway.....!!!    
    

    

FirstSelenium 
    Open Browser    http://google.com  chrome 
    Set Browser Implicit Wait    5
    Input Text    name=q    Bonjour Cyrille  
    Press Keys    name=q    ENTER   
    
    #Click Button    name=btnk   
    Sleep    3     
    Close Browser
    Log   Done 
    
    
#loop
    
 #   :FOR ${i}  IN ITEM   

Login
    LoginPageBlueway

Create_Ecran_from_Menu
    [Tags]        Create a New Ecran 
     LoginPageBlueway
     Designer_New_Ecran
     

Create_Ecran_from_Left_tree
    LoginPageBlueway
    Designer_New_Ecran_Left_tree
    

Create_Ecran_using_Class 
    LoginPageBlueway
    Designer_New_Ecran_using_Class

Driven_Data_Sample
    LoginPageBlueway 
    
*** Variables***
${Ecran_Name}         New_Ecran_is_ABC
${URL}  http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf 
${Credentials}   Nam  Son  Cyrille 

&{Login_by_id_Pass}     Username=admin    Password=admin 

${i}      Name of Ecran 
${ITEM}    Nam   Son  Cyrille 

  
*** Keyword***


    



Designer_New_Ecran_using_Class
    [Tags]   Create a new Ecran 
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton      
       
    Double Click Element    id=rigthmenu:idTopMenuItem:7:menuButton 
      
    #Wait Until Element Is Visible      xpath=//div[@id='tabscontent:tabView:BWconent_0:treeLeft_0:1:NodetreeLeft_0_']/div/label  
    #Open Context Menu    xpath=//div[@id='tabscontent:tabView:BWconent_0:treeLeft_0:1:NodetreeLeft_0_']/div/label  
    
    Sleep    3    
    #Open Context Menu    css:label[class="ui-outputlabel ui-widget"]  
     
    #Open Context Menu    xpath=//*[@class="ui-outputlabel ui-widget nodetree"]
    
            
    
    #Open Context Menu  xpath=//*[@class="ui-outputlabel ui-widget nodetree"]
    
    #OK
    Open Context Menu   xpath=//*[@id="tabscontent:tabView:BWconent_0:treeLeft_0:1"]
    
    #Open Context Menu    xpath=//*[@class="ui-tree-toggler ui-icon ui-icon-triangle-1-e bwroottreenode"]
    

    Wait Until Element Is Visible   xpath=(//a[contains(@href, '#')])[53]
    Click Link    xpath=(//a[contains(@href, '#')])[53]   
    
    Wait Until Element Is Visible    id=tabscontent:tabView:edittext_0_0 
    
    
    Click Element    id=tabscontent:tabView:edittext_0_0  
    
    #Input Text    locat//*[@class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all value-2-NOM_INTERFACE"]     Nam         
    Input Text    id=tabscontent:tabView:edittext_0_0     ${Ecran_Name}
    Input Text  tabscontent:tabView:edittext_0_0  ${Ecran_Name} 
      
     
    Click Element    css=.fa-floppy-o
    #Click Element    class=ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon   

    #Click Link    link=Save    

Designer_New_Ecran_Left_tree    
    [Tags]   Create a new Ecran 
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton      
       
    Double Click Element    id=rigthmenu:idTopMenuItem:7:menuButton 
      
    #Sleep    3    
    #Wait Until Element Is Visible      xpath=//div[@id='tabscontent:tabView:BWconent_0:treeLeft_0:1:NodetreeLeft_0_']/div/label  
    #Open Context Menu    xpath=//div[@id='tabscontent:tabView:BWconent_0:treeLeft_0:1:NodetreeLeft_0_']/div/label  
    
    #Wait Until Element Is Visible    xpath=//label[@class = 'ui-outputlabel ui-widget nodetree' and (text() = 'Screen' or . = 'Screen')]       
    #We use label Class  in Katalon
    #Click Element    xpath=//label[@class = 'ui-outputlabel ui-widget nodetree' and (text() = 'Screen' or . = 'Screen')] 
    
    # Nam Read Guru : Check by F12 : Complicate Xpath 
    Wait Until Element Is Visible  xpath=//label[text()='Screen' and @class='ui-outputlabel ui-widget nodetree']
    
    Open Context Menu    xpath=//label[text()='Screen' and @class='ui-outputlabel ui-widget nodetree'] 

    #Sleep    3    
    Wait Until Element Is Visible   xpath=(//a[contains(@href, '#')])[53]
    Click Link    xpath=(//a[contains(@href, '#')])[53]   
    #Click Element    xpath=//div[@id='tabscontent:tabView:BWconent_0:treeLeft_0:1:NodetreeLeft_0_']/div/label
    
   
    #Wait Until Page Contains Element     id=tabscontent:tabView:edittext_0_0     
    #Click Element    id=tabscontent:tabView:edittext_0_0    
    
    #Page Should Contain Textfield    id=tabscontent:tabView:edittext_0_0  
    Wait Until Element Is Visible    id=tabscontent:tabView:edittext_0_0 
    
    
    Click Element    id=tabscontent:tabView:edittext_0_0    
    Input Text    id=tabscontent:tabView:edittext_0_0     ${Ecran_Name}
    Input Text  tabscontent:tabView:edittext_0_0  ${Ecran_Name} 
    #Input Text    class=ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all value-2-NOM_INTERFACE    Nam    
    #Click Element    id=tabscontent:tabView:edittext_0_0
    #Wait Until Element Contains    id=tabscontent:tabView:edittext_0_0    ${Ecran_Name} 
    
    #Wait Until Page Contains    ${Ecran_Name} 
    #Current Frame Should Contain    New_Ecran_is_ABC  
    
     
    Click Element    css=.fa-floppy-o

    #Wait Until Element Is Visible    css=.fa-trash
    
LoginPageBlueway
     [Tags]    Login
    [Documentation]    Login Page 
    Open Browser    http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf    Chrome 
    #Set Browser Implicit Wait    7
    
    Click Element    id=loginForm:username    
    Input Text    id=loginForm:username    admin
    
    
    #Wait Until Element Is Visible    id=loginForm:password    
    Click Element    id=loginForm:password    
    Input Password    id=loginForm:password    &{Login_by_id_Pass}[Password]
    #Click Element   id=loginForm:j_idt20        
    
    #Click Element   class=ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btnConn
    
    #Click Element    css=.ui-button-text    
    
    Click Element    xpath=//button/span    
    Maximize Browser Window
    Sleep    5    

Designer_New_Ecran
    
    [Tags]   Create a new Ecran 
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton      
     

    #Open Context Menu    id=rigthmenu:idTopMenuItem:7:menuButton    
    
    
    
    Click Element    id=rigthmenu:idTopMenuItem:7:menuButton 
     
    #Click Element    xpath=//button[@alt='object']/span[@innertext='Objets'] 
    
    #Click Element    xpath=button[alt='object'] > .ui-button-text.ui-c    
 
    Sleep    3    
     #Wait Until Page Contains Element    xpath=//li[8]/div/div/div/div/div/div/ul/li[2]/div/div/div/label          
    #Click Element    id=rigthmenu:idTopMenuItem:7:j_idt428:1:j_idt432        
    
    #Element Should Be Focused  xpath=//li[8]/div/div/div/div/div/div/ul/li[2]/div/div/div/label

     #Click Element    class=ui-outputlabel ui-widget BWMN MenuItemIcon IECRAN
     #Click Element    xpath=//span[(text() = 'Screen' or . = 'Screen')]    
     
    #Click Element    xpath=//li[8]/div/div/div/div/div/div/ul/li[2]/div/div/div/label
     

    # We can use the Text : is OK    
    Click Element    xpath=//span[(text() = 'Screen' or . = 'Screen')]    


              

    # Not good - Because : Position changed
    #Click Element    xpath=//li[8]/div/div/div/div/div/div/ul/li[2]/div/div/div[2]/ul/li/span/span[3]/label
    
    #Not work
    #Click Element    xpath=//td[@role="gridcell" and @class="ui-panelgrid-cell"][1]    
    Sleep    2     
    
    #OK
    #Click Element    xpath=//li[8]/div/div/div/div/div/div/ul/li[2]/div/div/div[2]/ul/li/span/span[3]/label
    
    #Very Nice Code  From Katalon 
    #Click Element    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Screen'])[1]/following::label[1]               
    # Xpath is So Good :  (.//*[normalize-space(text()) and normalize-space(.)='Screen'])[1]  Cut  /following::label 1
    
    
    #Relative xapth helper : Good 
    #Click Element    xpath=(//SPAN[@class='ui-treenode-icon ui-icon bwicon-support '])[39]  
    
    Click Element        xpath=(//LABEL[@title=''][text()='New'][text()='New'])[33] 
    
                              
    
    #Katalon 
    #Click Element    xpath=(.//label[@class = 'customDrag  ' and (text() = 'New' or . = 'New')])    
    

   # Chơi chiêu đóng Ngoặc vô    (.//label[@class = 'customDrag  ' and (text() = 'New' or . = 'New')])[1]  

    #Click Element    xpath=//label[text() = 'New' ]
    

          

    #Click Element    xpath=//span[contains(@class,'ui-treenode-label ui-corner-all ui-state-highlight')][label[contains(@class,'')][contains(text(),'New')]  
    
    #Katalon    xpath=//li[8]/div/div/div/div/div/div/ul/li[2]/div/div/div[2]/ul/li/span/span[3]/label
                     
                      

    Sleep    2    
    
    #Click Element    id^="rigthmenu:idTopMenuItem:7:j_id
    #Click Element    xpath=//li[@id^='rigthmenu:idTopMenuItem:7:j_idt428:1:j_idt436:0']/span/span[2]   
    
    #Click Element   xpath=//*[normalize-space(text()) and normalize-space(.)='Screen'])[1]/following::span[3]    
   
    #Click Element    xpath=//span[@class = 'ui-treenode-icon ui-icon bwicon-support ']    
    
    #Click Element    xpath=//button[@id='rigthmenu:idTopMenuItem:7:menuButton']/span    
   
    #Sleep    3    
    Wait Until Page Contains Element     id=tabscontent:tabView:edittext_0_0     
    Click Element    id=tabscontent:tabView:edittext_0_0    
    
    Input Text    id=tabscontent:tabView:edittext_0_0    ${Ecran_Name}
    
    Click Element    css=.fa-floppy-o
    
    #Click Element    css=.ui-state-hover > .ui-menuitem-text    
    
    

    
     #F12 : Find CSS   
     #Second: Selenium IDE  
     #Third Katalon SEcond 
     

     # Element Should Contain locator, expected, message=
     # Suppose you are expecting a particular locator to get value as "Payment was successful", so firstly store that in temp variable "response" and finds its value by "Get Text" and then compare it.

     #${response}    Get Text    xpath=/html/body/div/tr[2]/td/strong
     #Should Be Equal As Strings    ${response}    Payment was successful
     
      





