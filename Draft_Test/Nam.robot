*** setting ***
Library    SeleniumLibrary     

*** Variables ***
${DATE}    2011-06-27

*** Test Cases ***
Example
    I type ${1} + ${2}
    Today is ${DATE}
    

*** Keywords ***
I execute "${cmd:[^"]+}"
    Run Process    ${cmd}    shell=True

I execute "${cmd}" with "${opts}"
    Run Process    ${cmd} ${opts}    shell=True

I type ${num1:\d+} ${operator:[+-]} ${num2:\d+}
    Calculate    ${num1}    ${operator}    ${num2}

Today is ${date:\d{4}-\d{2}-\d{2}}
    Log    ${DATE}
    


    