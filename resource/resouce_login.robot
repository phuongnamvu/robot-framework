
*** setting ***
Library    SeleniumLibrary       

*** Variables***
${Ecran_Name}         New_Ecran_is_ABC
${URL}  http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf 
${Credentials}   Nam  Son  Cyrille 
&{Login_by_id_Pass}     Username=admin    Password=admin 

*** Keyword***
   
LoginPageBlueway
     [Tags]    Login
    [Documentation]    Login Page 
    Open Browser    http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf    Chrome     
    Click Element    id=loginForm:username    
    Input Text    id=loginForm:username    admin    
    Click Element    id=loginForm:password    
    Input Password    id=loginForm:password    &{Login_by_id_Pass}[Password]   
    Click Element    xpath=//button/span    
    Maximize Browser Window
    
Designer_New_Ecran
    [Tags]   Create a new Ecran 
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Click Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Sleep    3        
    Click Element    xpath=//span[(text() = 'Screen' or . = 'Screen')]       
    Sleep    2      
    Click Element        xpath=(//LABEL[@title=''][text()='New'][text()='New'])[33]      
    Sleep    2   
    Wait Until Page Contains Element     id=tabscontent:tabView:edittext_0_0     
    Click Element    id=tabscontent:tabView:edittext_0_0    
    Input Text    id=tabscontent:tabView:edittext_0_0    ${Ecran_Name}
    Click Element    css=.fa-floppy-o
    
 




